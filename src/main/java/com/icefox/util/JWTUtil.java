package com.icefox.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class JWTUtil {

    // 签名密钥
    private static final String SECRET = "@#$*&..jjy520QAQXTZYC";

    /**
     * 生成token
     *
     * @param payload token携带的信息
     * @return token字符串
     */
    public static String getToken(Map<String, String> payload) {
        // 指定token过期时间为7天
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 7);
        JWTCreator.Builder builder = JWT.create();
        // 构建payload
        payload.forEach((k, v) -> builder.withClaim(k, v));
        // 指定过期时间和签名算法
        String token = builder.withExpiresAt(calendar.getTime()).sign(Algorithm.HMAC256(SECRET));
        return token;
    }


    /**
     * 解析token
     *
     * @param token token字符串
     * @return 解析后的token
     */
    public static DecodedJWT decodeToken(String token) {
        JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
        DecodedJWT decodedJWT = jwtVerifier.verify(token);
        return decodedJWT;
    }

    /**
     * 验证JWT令牌是否有效
     *
     * @param token JWT令牌字符串
     * @return 如果令牌有效返回true，否则返回false
     */
    public static boolean isTokenValid(String token) {
        try {
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
            DecodedJWT jwt = jwtVerifier.verify(token);
            // 检查是否过期（可选，因为JWTVerifier在令牌过期时会抛出异常）
            Date expiration = jwt.getExpiresAt();
            return expiration == null || expiration.after(new Date());
        } catch (JWTVerificationException e) {
            // JWTVerifier在签名不正确或过期时会抛出此异常
            return false;
        }
    }

    //获取payload中的某个值
    public static String getPayloadValue(String token, String key) {
        DecodedJWT decodedJWT = decodeToken(token);
        return decodedJWT.getClaim(key).asString();
    }
}

