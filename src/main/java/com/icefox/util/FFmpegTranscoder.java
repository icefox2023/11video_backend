package com.icefox.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class FFmpegTranscoder {

    private static final Logger log = LoggerFactory.getLogger(FFmpegTranscoder.class);

    public static void execVideoTranscode(File ffmpegCmd, String sourceFile, String targetFile) {
        try {
            log.info("<<开始视频转码>> 文件名：{}", sourceFile);
            long startTime = System.currentTimeMillis();
            String cmd = ffmpegCmd.getAbsolutePath() + " -y -i " + sourceFile + " -vcodec libx264 -vf scale=\"iw/1:ih/1\" " + targetFile;
            log.info("<<命令>> {}", cmd);

            Process process = Runtime.getRuntime().exec(cmd);

            // 通过读取进程的流信息，可以看到视频转码的相关执行信息，并且使得下面的视频转码时间贴近实际的情况
            BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            String line = null;
            while ((line = br.readLine()) != null) {
                log.debug("<<视频执行信息>> {}", line);
            }
            br.close();

            // 注意：通常不需要手动关闭 process.getOutputStream()，因为你没有向它写入数据
            // process.getOutputStream().close(); // 可以注释掉这行

            // 你可能想等待进程完成，然后读取其输出（如果有的话）
            // int exitCode = process.waitFor(); // 可以添加这行代码来等待进程完成

            log.info("<<开始关闭进程相关流>>");
            // process.getInputStream().close(); // 如果不需要读取输出，可以注释掉这行
            process.getErrorStream().close();

            long endTime = System.currentTimeMillis();
            log.info("<<视频转码完成>> 耗时 {}ms", (endTime - startTime));
        } catch (IOException e) {
            log.error("<<视频转码失败，原因：发生IO异常>>", e);
        }
    }
}
