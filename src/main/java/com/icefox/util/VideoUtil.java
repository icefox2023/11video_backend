package com.icefox.util;

import lombok.extern.slf4j.Slf4j;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import java.io.File;

@Slf4j
public class VideoUtil {

    // 上传文件的存储目录（请确保这个目录存在且应用有写入权限）
    private static final String UPLOAD_DIR = System.getProperty("user.dir") + "\\src\\main\\resources\\static\\video\\";

    // 缓冲区大小（可以根据需要调整）
//    private static final int BUFFER_SIZE = 4096; // 4KB


    /**
     * 上传视频文件到指定目录
     *
     * @param file 上传的视频文件（MultipartFile）
     * @return 文件名（不包含路径）或 null（上传失败）
     */
    public static String uploadVideo(MultipartFile file) {
        long startTime = System.currentTimeMillis();
        try {
            // 检查文件是否为空
            if (file.isEmpty()) {
                return null;
            }

            // 检查文件类型是否为视频
            String contentType = file.getContentType();
            assert contentType != null;
            if (!contentType.startsWith("video/")) {
                return null;
            }

            // 生成唯一的文件名
            String originalFilename = file.getOriginalFilename();
            String extension = "";
            assert originalFilename != null;
            if (!originalFilename.isEmpty()) {
                int dotIndex = originalFilename.lastIndexOf('.');
                if (dotIndex > 0) {
                    extension = originalFilename.substring(dotIndex);
                }
            }
            String fileName = UUID.randomUUID() + extension;

            // 生成文件存储路径
            Path targetLocation = Paths.get(UPLOAD_DIR, fileName);

//            // 使用缓冲流来保存文件到指定位置
//            try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetLocation.toFile()), BUFFER_SIZE);
//                 BufferedInputStream bis = new BufferedInputStream(file.getInputStream(), BUFFER_SIZE)) {
//
//                byte[] buffer = new byte[BUFFER_SIZE];
//                int bytesRead;
//                while ((bytesRead = bis.read(buffer)) != -1) {
//                    bos.write(buffer, 0, bytesRead);
//                }
//            log.info("上传视频文件成功，耗时：" + (System.currentTimeMillis() - startTime) + "ms");
//                // 返回上传成功的文件名
//                return fileName;
//            }

            // 使用MultipartFile的transferTo方法保存文件
            file.transferTo(targetLocation.toFile());
            log.info(fileName + "视频文件上传成功，耗时：" + (System.currentTimeMillis() - startTime) + "ms");
            // 返回上传成功的文件名
            return fileName;
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 获取已上传视频文件的资源（用于下载或展示）
     *
     * @param fileName 视频文件名
     * @return 视频文件资源（FileSystemResource）或错误响应
     */
    public static ResponseEntity<?> getUploadedVideo(String fileName) {
        try {
            Path filePath = Paths.get(UPLOAD_DIR, fileName);
            File file = filePath.toFile();

            // 检查文件是否存在
            if (!file.exists() || !file.isFile()) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            // 返回视频文件资源
            return ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"")
                    .body(new FileSystemResource(file));
        } catch (Exception e) {
            // 处理异常，例如文件访问失败
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 获取视频时长（单位：秒）
     */
    public static Long getVideoDuration(String filePath) {

        try {
            FFmpegFrameGrabber grabber = FFmpegFrameGrabber.createDefault(filePath);
            grabber.start();
            Long duration = grabber.getLengthInTime() / (1000 * 1000);
            grabber.stop();
            return duration;
        } catch (Exception e) {
            System.out.println("获取视频时长失败,原因：" + e.getMessage());
        }
        return null;
    }

}