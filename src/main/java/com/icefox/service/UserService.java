package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.User;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;

import java.util.List;

public interface UserService extends IService<User> {

    int addUser(User user);

    User getUserById(Long id);

    int updateUser(User user);

    User login(User user);

    int updateIcon(String icon, Long id);

    //用户列表
    PageDTO<User> getUserList(PageQuery userQuery);

    //关注列表
    PageDTO<User> getFollowList(PageQuery userQuery, List<Long> followIdList);

    //粉丝列表
    PageDTO<User> getFansList(PageQuery userQuery, List<Long> fansIdList);
}
