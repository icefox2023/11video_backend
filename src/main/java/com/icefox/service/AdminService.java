package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.Admin;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;

public interface AdminService extends IService<Admin> {

    Admin login(Admin admin);

    int addAdmin(Admin admin);

    int deleteAdmin(Long id);

    int updateAdmin(Admin admin);

    //管理员列表
    PageDTO<Admin> getAdminList(PageQuery adminQuery);
}
