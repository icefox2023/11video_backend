package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;
import java.util.List;

public interface VideoService extends IService<Video> {
    int addVideo(Video video);

    Video getVideoById(Long id);

    int deleteVideo(Long id);

    int updateVideo(Video video);

    int updateThumb(Long id, String thumbUrl);

    //综合排序
    PageDTO<Video> getVideoListByPage(PageQuery videoQuery);

    //按时间排序
    PageDTO<Video> getVideoListByPageOrderByTime(PageQuery videoQuery);

    //按热度排序
    PageDTO<Video> getVideoListByPageOrderByHot(PageQuery videoQuery);

    int likeVideo(Long id);

    int cancelLikeVideo(Long id);


    //视频搜索
    PageDTO<Video> searchVideo(PageQuery videoQuery, String keyword);

    //视频分类搜索
    PageDTO<Video> searchVideoByCategory(PageQuery videoQuery, Long categoryId);

    //个人草稿视频时间倒序排序
    PageDTO<Video> getDraftVideoListByPageOrderByTime(PageQuery videoQuery, Long userId);

    //个人视频时间倒序排序
    PageDTO<Video> getVideoListByPageOrderByTime(PageQuery videoQuery, Long userId);

    //个人视频收藏时间倒序排序
    PageDTO<Video> getCollectVideoListByPageOrderByTime(PageQuery videoQuery, List<Long> videoIdList);

    //待审核视频列表
    PageDTO<Video> getWaitAuditVideoList(PageQuery videoQuery);
}
