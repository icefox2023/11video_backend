package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.relation.Focus;

import java.util.List;

public interface FocusService extends IService<Focus> {
    //获取关注id列表
    List<Long> getFocusIdList(Long userId);

    //获取粉丝id列表
    List<Long> getFansIdList(Long userId);

    //是否关注
    boolean isFocus(Long userId, Long focusId);
}
