package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.relation.Collection;

import java.util.List;

public interface CollectionService extends IService<Collection> {
    boolean isCollected(Long userId, Long videoId);

    Collection FindIdByUserIdAndVideoId(Long userId, Long videoId);

    //查找用户收藏的视频
    List<Long> FindCollectedVideoIdByUserId(Long userId);
}
