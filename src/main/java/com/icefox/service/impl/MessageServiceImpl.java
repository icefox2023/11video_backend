package com.icefox.service.impl;

import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.MessageMapper;
import com.icefox.pojo.Message;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.MessageService;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {

    //获取聊天记录
    @Override
    public PageDTO<Message> getChatMessages(PageQuery pageQuery, Long senderId, Long receiverId) {
        Page<Message> chatPage = pageQuery.toMpPage(new OrderItem("msg_send_date", false));
        lambdaQuery().eq(Message::getMsgTypeId, 1).eq(Message::getMsgSendUserId, senderId).eq(Message::getMsgReceiveUserId, receiverId).or().eq(Message::getMsgTypeId, 1).eq(Message::getMsgSendUserId, receiverId).eq(Message::getMsgReceiveUserId, senderId).page(chatPage);
        return PageDTO.of(chatPage, Message.class);
    }

    //获取系统消息
    @Override
    public PageDTO<Message> getSystemMessages(PageQuery pageQuery, Long receiverId) {
        Page<Message> systemPage = pageQuery.toMpPage(new OrderItem("msg_send_date", false));
        lambdaQuery().eq(Message::getMsgTypeId, 2).eq(Message::getMsgReceiveUserId, receiverId).page(systemPage);
        return PageDTO.of(systemPage, Message.class);
    }

//    //添加聊天记录
//    @Override
//    public void addChatMessage(Message message) {
//
//        baseMapper.addChatMessage(message);
//    }
//
//    //添加系统消息
//    @Override
//    public void addSystemMessage(Message message) {
//
//        baseMapper.addSystemMessage(message);
//    }
//
//    //添加评论
//    @Override
//    public void addComment(Message message) {
//
//        baseMapper.addCommentMessage(message);
//    }

    //获取视频评论
    @Override
    public PageDTO<Message> getComments(PageQuery pageQuery, Long videoId) {
        Page<Message> commentPage = pageQuery.toMpPage(new OrderItem("msg_send_date", true));
        lambdaQuery().eq(Message::getMsgVideoId, videoId).eq(Message::getMsgTypeId, 3).page(commentPage);
        return PageDTO.of(commentPage, Message.class);
    }

    //删除消息
    @Override
    public void deleteComment(Long commentId) {
        baseMapper.deleteById(commentId);
    }
}
