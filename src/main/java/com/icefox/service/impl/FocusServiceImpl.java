package com.icefox.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.FocusMapper;
import com.icefox.pojo.relation.Focus;
import com.icefox.service.FocusService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FocusServiceImpl extends ServiceImpl<FocusMapper, Focus> implements FocusService {
    @Override
    public List<Long> getFocusIdList(Long userId) {
        return baseMapper.getFocusIdList(userId);
    }

    @Override
    public List<Long> getFansIdList(Long userId) {
        return baseMapper.getFanIdList(userId);
    }

    @Override
    public boolean isFocus(Long userId, Long focusId) {
        return baseMapper.isFocus(userId, focusId);
    }
}
