package com.icefox.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.CollectionMapper;
import com.icefox.pojo.relation.Collection;
import com.icefox.service.CollectionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl extends ServiceImpl<CollectionMapper, Collection> implements CollectionService {

    @Override
    public boolean isCollected(Long userId, Long videoId) {
        return baseMapper.isCollected(userId, videoId);
    }

    @Override
    public Collection FindIdByUserIdAndVideoId(Long userId, Long videoId) {
        return baseMapper.findCollectionIdByUserIdAndVideoId(userId, videoId);
    }

    @Override
    public List<Long> FindCollectedVideoIdByUserId(Long userId) {
        return baseMapper.findCollectedVideoIdByUserId(userId);
    }
}
