package com.icefox.service.impl;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.icefox.mapper.VideoMapper;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.VideoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


@Service
public class VideoServiceImpl extends ServiceImpl<VideoMapper, Video> implements VideoService {

    @Override
    public int addVideo(Video video) {
        return baseMapper.insert(video);
    }

    @Override
    public Video getVideoById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public int deleteVideo(Long id) {
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateVideo(Video video) {
        return baseMapper.updateById(video);
    }

    @Override
    public int updateThumb(Long id, String thumbUrl) {
        return baseMapper.updateThumb(id, thumbUrl);
    }

    // 已发布视频列表综合排序
    @Override
    public PageDTO<Video> getVideoListByPage(PageQuery videoQuery) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("view_num", false), new OrderItem("edit_date", false), new OrderItem("like_num", false), new OrderItem("collect_num", false));
        lambdaQuery().eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    //已发布视频列表按时间排序
    @Override
    public PageDTO<Video> getVideoListByPageOrderByTime(PageQuery videoQuery) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("edit_date", false));
        lambdaQuery().eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    // 已发布视频列表按热度排序
    @Override
    public PageDTO<Video> getVideoListByPageOrderByHot(PageQuery videoQuery) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("view_num", false));
        lambdaQuery().eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    @Override
    public int likeVideo(Long id) {
        UpdateWrapper<Video> updateWrapper = new UpdateWrapper<>();
        updateWrapper.setSql("like_num = like_num + 1").eq("video_id", id);
        return baseMapper.update(null, updateWrapper);
    }

    @Override
    public int cancelLikeVideo(Long id) {
        UpdateWrapper<Video> updateWrapper = new UpdateWrapper<>();
        updateWrapper.setSql("like_num = like_num - 1").eq("video_id", id);
        return baseMapper.update(null, updateWrapper);
    }

    //搜索已发布视频
    @Override
    public PageDTO<Video> searchVideo(PageQuery videoQuery, String keyword) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("view_num", false), new OrderItem("edit_date", false), new OrderItem("like_num", false), new OrderItem("collect_num", false));
        lambdaQuery().like(Video::getVideoTitle, keyword).eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    //搜索已发布视频按分类
    @Override
    public PageDTO<Video> searchVideoByCategory(PageQuery videoQuery, Long categoryId) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("view_num", false), new OrderItem("edit_date", false), new OrderItem("like_num", false), new OrderItem("collect_num", false));
        lambdaQuery().eq(Video::getVideoTypeId, categoryId).eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    //搜索个人视频草稿
    @Override
    public PageDTO<Video> getDraftVideoListByPageOrderByTime(PageQuery videoQuery, Long userId) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("edit_date", false));
        lambdaQuery().eq(Video::getUserId, userId).eq(Video::getVideoStateId, 5).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    //搜索个人视频
    @Override
    public PageDTO<Video> getVideoListByPageOrderByTime(PageQuery videoQuery, Long userId) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("edit_date", false));
        lambdaQuery().eq(Video::getUserId, userId).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    //搜索收藏视频
    @Override
    public PageDTO<Video> getCollectVideoListByPageOrderByTime(PageQuery videoQuery, List<Long> videoIdList) {
        Page<Video> videoPage = videoQuery.toMpPage(new OrderItem("edit_date", false));
        // 添加视频ID列表的查询条件 这里的in查询会自动使用索引
        lambdaQuery().in(Video::getVideoId, videoIdList).eq(Video::getVideoStateId, 2).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }

    @Override
    public PageDTO<Video> getWaitAuditVideoList(PageQuery videoQuery) {
        Page<Video> videoPage = videoQuery.toMpPage();
        lambdaQuery().eq(Video::getVideoStateId, 1).page(videoPage);
        return PageDTO.of(videoPage, Video.class);
    }


}
