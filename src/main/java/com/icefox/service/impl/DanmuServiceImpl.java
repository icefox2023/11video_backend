package com.icefox.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.DanmuMapper;
import com.icefox.pojo.Danmu;
import com.icefox.service.DanmuService;
import org.springframework.stereotype.Service;

@Service
public class DanmuServiceImpl extends ServiceImpl<DanmuMapper, Danmu> implements DanmuService {
}
