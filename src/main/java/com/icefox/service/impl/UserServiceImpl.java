package com.icefox.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.UserMapper;
import com.icefox.pojo.User;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.FocusService;
import com.icefox.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private FocusService focusService;

    @Override
    public int addUser(User user) {
        return baseMapper.insert(user);
    }

    @Override
    public User getUserById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public int updateUser(User user) {
        return baseMapper.updateById(user);
    }

    @Override
    public User login(User user) {
        return baseMapper.login(user);
    }

    @Override
    public int updateIcon(String icon, Long id) {
        return baseMapper.updateIcon(icon, id);
    }

    //用户列表
    @Override
    public PageDTO<User> getUserList(PageQuery userQuery) {
        Page<User> userPage = userQuery.toMpPage();
        lambdaQuery().page(userPage);
        return PageDTO.of(userPage, User.class);
    }

    //关注列表
    @Override
    public PageDTO<User> getFollowList(PageQuery userQuery, List<Long> focusIdList) {
        Page<User> userPage = userQuery.toMpPage();
        lambdaQuery().in(User::getUserId, focusIdList).page(userPage);
        return PageDTO.of(userPage, User.class);
    }

    //粉丝列表
    @Override
    public PageDTO<User> getFansList(PageQuery userQuery, List<Long> fansIdList) {
        Page<User> userPage = userQuery.toMpPage();
        lambdaQuery().in(User::getUserId, fansIdList).page(userPage);
        return PageDTO.of(userPage, User.class);
    }
}
