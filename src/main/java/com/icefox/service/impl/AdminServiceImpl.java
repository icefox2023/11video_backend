package com.icefox.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.icefox.mapper.AdminMapper;
import com.icefox.pojo.Admin;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.AdminService;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

    @Override
    public Admin login(Admin admin) {
        return baseMapper.adminLogin(admin);
    }

    @Override
    public int addAdmin(Admin admin) {
        return baseMapper.insert(admin);
    }

    @Override
    public int deleteAdmin(Long id) {
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateAdmin(Admin admin) {
        return baseMapper.update(admin, new UpdateWrapper<Admin>().eq("admin_username", admin.getAdminUsername()).set("admin_password", admin.getAdminPassword()));
    }

    @Override
    public PageDTO<Admin> getAdminList(PageQuery adminQuery) {
        Page<Admin> adminPage = adminQuery.toMpPage();
        lambdaQuery().page(adminPage);
        return PageDTO.of(adminPage, Admin.class);
    }

}
