package com.icefox.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.icefox.pojo.Message;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.query.PageQuery;

public interface MessageService extends IService<Message> {
    //获取聊天消息
    PageDTO<Message> getChatMessages(PageQuery pageQuery, Long senderId, Long receiverId);

    //获取系统消息
    PageDTO<Message> getSystemMessages(PageQuery pageQuery, Long receiverId);

//    //添加聊天消息
//    void addChatMessage(Message message);
//
//    //添加系统消息
//    void addSystemMessage(Message message);
//
//    //添加评论
//    void addComment(Message message);

    //获取评论列表
    PageDTO<Message> getComments(PageQuery pageQuery, Long videoId);

    //删除评论
    void deleteComment(Long commentId);
}
