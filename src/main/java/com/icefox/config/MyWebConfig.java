package com.icefox.config;

import com.icefox.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebConfig implements WebMvcConfigurer {

    @Autowired
    private LoginInterceptor loginInterceptor;

    //静态资源映射
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/doc.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/static/video/**").addResourceLocations("file:D:\\idea-workspace\\11video\\src\\main\\resources\\static\\video\\");
        registry.addResourceHandler("/static/icon/**").addResourceLocations("file:D:\\idea-workspace\\11video\\src\\main\\resources\\static\\icon\\");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 对swagger的请求不进行拦截
        String[] excludePatterns = new String[]{"/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**",
                "/api", "/api-docs", "/api-docs/**", "/doc.html/**"};
        //注册LoginInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(loginInterceptor);
        registration.addPathPatterns("/**")
                .excludePathPatterns("/resources/**")
                .excludePathPatterns("/error/**")
                .excludePathPatterns("/user/addUser")
                .excludePathPatterns("/user/{id}")
                .excludePathPatterns("/video/**")
                .excludePathPatterns(excludePatterns);
    }
}
