package com.icefox.controller;

import com.icefox.pojo.Admin;
import com.icefox.pojo.Message;
import com.icefox.pojo.User;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.ResultDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.*;
import com.icefox.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.naming.AuthenticationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/admin")
@Api(tags = "管理员管理")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private UserService userService;

    @Autowired
    private DanmuService danmuService;

    // 管理员登录
    @ApiOperation("管理员登录")
    @PostMapping("/login")
    public ResultDTO<Map<String, Object>> adminLogin(@RequestBody Admin admin) {
        log.info("管理员[{}]登录", admin.getAdminUsername());
        Map<String, Object> result = new HashMap<>();
        try {
            Admin adminDB = adminService.login(admin);
            if (adminDB == null) {
                throw new AuthenticationException("用户名或密码错误！");
            }
            Map<String, String> payload = new HashMap<>();
            payload.put("id", adminDB.getAdminId().toString());
            payload.put("name", adminDB.getAdminUsername());
            // 生成jwt令牌
            String token = JWTUtil.getToken(payload);
            result.put("state", true);
            result.put("msg", "登录成功！");
            result.put("token", token); // 响应token
        } catch (AuthenticationException e) {
            result.put("state", false);
            result.put("msg", e.getMessage());
        } catch (Exception e) {
            // 处理其他异常
            result.put("state", false);
            result.put("msg", "认证过程中发生错误：" + e.getMessage());
        }
        return ResultDTO.success(result);
    }

    // 添加管理员
    @ApiOperation("添加管理员")
    @PostMapping("/addAdmin")
    public ResultDTO addAdmin(@RequestBody Admin admin) {
        try {
            adminService.addAdmin(admin);
            return ResultDTO.success("添加管理员" + admin.getAdminUsername() + "成功！");
        } catch (Exception e) {
            return ResultDTO.error("添加管理员失败！");
        }
    }

    // 删除管理员
    @ApiOperation("删除管理员")
    @DeleteMapping("/deleteAdmin/{id}")
    public ResultDTO deleteAdmin(@PathVariable("id") Long id) {
        try {
            adminService.deleteAdmin(id);
            return ResultDTO.success("删除管理员成功！");
        } catch (Exception e) {
            return ResultDTO.error("删除管理员失败！");
        }
    }

    // 更新管理员信息
    @ApiOperation("更新管理员信息(修改密码)")
    @PutMapping("/updateAdmin")
    public ResultDTO updateAdmin(@RequestBody Admin admin) {
        try {
            adminService.updateAdmin(admin);
            return ResultDTO.success("更新管理员信息成功！");
        } catch (Exception e) {
            return ResultDTO.error("更新管理员信息失败！");
        }
    }

    // 查询管理员信息
    @ApiOperation("查询管理员信息")
    @GetMapping("/getAdmin/{id}")
    public Admin getAdmin(@PathVariable("id") Long id) {
        return adminService.getById(id);
    }

    // 视频审核通过
    @ApiOperation("视频审核通过")
    @PutMapping("/passVideo/{id}")
    public ResultDTO passVideo(@PathVariable("id") Long id) {
        try {
            Video video = videoService.getById(id);
            if (video.getVideoStateId() == 2L) {
                return ResultDTO.error("视频已审核通过！");
            }
            video.setVideoStateId(2L);
            videoService.updateVideo(video);
            Message message = new Message();
            message.setMsgTitle("系统提醒");
            message.setMsgContent("您好！您的视频《" + video.getVideoTitle() + "》审核通过！");
            message.setMsgSendDate(new Date());
            message.setMsgTypeId(2L);
            message.setMsgReceiveUserId(video.getUserId());
            message.setMsgVideoId(video.getVideoId());
            messageService.save(message);
        } catch (Exception e) {
            return ResultDTO.error("审核通过失败！");
        }
        return ResultDTO.success("审核通过！");
    }

    // 视频审核不通过
    @ApiOperation("视频审核不通过")
    @PutMapping("/rejectVideo/{id}")
    public ResultDTO rejectVideo(@PathVariable("id") Long id) {
        try {
            Video video = videoService.getById(id);
            if (video.getVideoStateId() == 3L) {
                return ResultDTO.error("视频已审核不通过！");
            }
            video.setVideoStateId(3L);
            videoService.updateVideo(video);
            Message message = new Message();
            message.setMsgTitle("系统提醒");
            message.setMsgContent("抱歉！您的视频《" + videoService.getVideoById(id).getVideoTitle() + "》审核未通过！");
            message.setMsgSendDate(new Date());
            message.setMsgTypeId(2L);
            message.setMsgReceiveUserId(video.getUserId());
            message.setMsgVideoId(video.getVideoId());
            messageService.save(message);
        } catch (Exception e) {
            return ResultDTO.error("审核不通过失败！");
        }
        return ResultDTO.success("审核不通过！");
    }

    // 视频下架
    @ApiOperation("视频下架")
    @PutMapping("/offShelfVideo/{id}")
    public ResultDTO offShelfVideo(@PathVariable("id") Long id) {
        try {
            Video video = videoService.getById(id);
            if (video.getVideoStateId() == 4L) {
                return ResultDTO.error("视频已下架！");
            }
            video.setVideoStateId(4L);
            videoService.updateVideo(video);
            Message message = new Message();
            message.setMsgTitle("系统提醒");
            message.setMsgContent("抱歉！您的视频《" + videoService.getVideoById(id).getVideoTitle() + "》因违规已下架！");
            message.setMsgSendDate(new Date());
            message.setMsgTypeId(2L);
            message.setMsgReceiveUserId(videoService.getVideoById(id).getUserId());
            message.setMsgVideoId(id);
            messageService.save(message);
        } catch (Exception e) {
            return ResultDTO.error("下架失败！");
        }
        return ResultDTO.success("下架成功！");
    }

    // 封禁用户
    @ApiOperation("封禁用户")
    @PutMapping("/blockUser/{id}")
    public ResultDTO blockUser(@PathVariable("id") Long id) {
        try {
            User user = userService.getUserById(id);
            if (user.getUserState() == 0L) {
                return ResultDTO.error("用户已被封禁！");
            }
            user.setUserState(0L);
            userService.updateUser(user);
        } catch (Exception e) {
            return ResultDTO.error("封禁用户失败！");
        }
        return ResultDTO.success("封禁用户成功！");
    }

    // 解封用户
    @ApiOperation("解封用户")
    @PutMapping("/unblockUser/{id}")
    public ResultDTO unblockUser(@PathVariable("id") Long id) {
        try {
            User user = userService.getUserById(id);
            if (user.getUserState() == 1L) {
                return ResultDTO.error("用户已解封！");
            }
            user.setUserState(1L);
            userService.updateUser(user);
        } catch (Exception e) {
            return ResultDTO.error("解封用户失败！");
        }
        return ResultDTO.success("解封用户成功！");
    }

    //违规评论删除
    @ApiOperation("违规评论删除")
    @DeleteMapping("/deleteComment/{id}")
    public ResultDTO deleteComment(@PathVariable("id") Long id) {
        try {
            Video video = videoService.getVideoById(id);
            video.setCommentNum(video.getCommentNum() - 1);
            videoService.updateVideo(video);
            messageService.deleteComment(id);
        } catch (Exception e) {
            return ResultDTO.error("违规评论删除失败！");
        }
        return ResultDTO.success("违规评论删除成功！");
    }

    //管理员列表
    @ApiOperation("管理员列表")
    @GetMapping("/adminList")
    public ResultDTO adminList(PageQuery adminQuery) {
        try {
            return ResultDTO.success("管理员列表获取成功！", adminService.getAdminList(adminQuery));
        } catch (Exception e) {
            return ResultDTO.error("管理员列表获取失败！");
        }
    }

    //删除违规弹幕
    @ApiOperation("删除违规弹幕")
    @DeleteMapping("/deleteIllegalDanmu/{id}")
    public ResultDTO deleteIllegalDanmu(@PathVariable("id") Long id) {
        try {
            danmuService.removeById(id);
        } catch (Exception e) {
            return ResultDTO.error("删除违规弹幕失败！");
        }
        return ResultDTO.success("删除违规弹幕成功！");
    }
}
