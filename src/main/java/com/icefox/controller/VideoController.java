package com.icefox.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.icefox.pojo.Message;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.ResultDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.pojo.relation.Collection;
import com.icefox.service.CollectionService;
import com.icefox.service.MessageService;
import com.icefox.service.VideoService;
import com.icefox.util.FFmpegTranscoder;
import com.icefox.util.JWTUtil;
import com.icefox.util.VideoUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;
import java.util.UUID;

@Api(tags = "视频管理")
@Slf4j
@RestController
@RequestMapping("/video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private MessageService messageService;

    //视频上传
    @ApiOperation(value = "上传视频")
    @RequestMapping(value = "uploadVideo", method = RequestMethod.POST)
    public ResultDTO uploadVideo(@RequestPart(value = "videoFile") MultipartFile videoFile, Video video, HttpServletRequest request) {
        log.info(video.toString());
        String sourceVideo = VideoUtil.uploadVideo(videoFile);
        int dotIndex = sourceVideo.lastIndexOf('.');
        String sourceNameWithoutExt = sourceVideo.substring(0, dotIndex);
        String sourcePath = System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceVideo;
        if (!sourceVideo.toLowerCase().endsWith(".mp4")) {
            String ffmpegPath = System.getenv("FFMPEG_HOME");
            File ffmpegCmd = new File(ffmpegPath + "/bin/ffmpeg.exe");
            FFmpegTranscoder.execVideoTranscode(ffmpegCmd, sourcePath, System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceNameWithoutExt + ".mp4");
//            sourcePath = System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceNameWithoutExt + ".mp4";
        }
        String userId = JWTUtil.getPayloadValue(request.getHeader("token"), "id");
        video.setUserId(Long.parseLong(userId));
        video.setVideoUrl("/video/" + sourceNameWithoutExt + ".mp4");
//        video.setDuration(VideoUtil.getVideoDuration(sourcePath));
        video.setEditDate(new Date());
        video.setCollectNum(0);
        video.setViewNum(0);
        video.setLikeNum(0);
        video.setCommentNum(0);
        video.setThumbnailUrl("/thumbnail/default.jpg");
//        video.setVideoTypeId(0L);
//        video.setVideoStateId(1L);
        try {
            videoService.addVideo(video);
            log.info("上传视频成功！");
            return ResultDTO.success("上传视频成功", video);
        } catch (Exception e) {
            log.error("上传视频失败！", e);
            return ResultDTO.error("上传视频失败！");
        }
    }

    // 视频播放
    @ApiOperation(value = "视频播放")
    @GetMapping("{id}/play")
    public void play(@PathVariable("id") Long id, HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.reset();
        String path = System.getProperty("user.dir");
        Video video = videoService.getVideoById(id);
        File file = new File(path + "/src/main/resources/static" + video.getVideoUrl());
        long fileLength = file.length();
        // 随机读文件
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");

        //获取从那个字节开始读取文件
        String rangeString = request.getHeader("Range");
        long range = 0;
        if (StrUtil.isNotBlank(rangeString)) {
            range = Long.valueOf(rangeString.substring(rangeString.indexOf("=") + 1, rangeString.indexOf("-")));
        }
        //获取响应的输出流
        OutputStream outputStream = response.getOutputStream();
        //设置内容类型
        response.setHeader("Content-Type", "video/mp4");
        //返回码需要为206，代表只处理了部分请求，响应了部分数据
        response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);

        // 移动访问指针到指定位置
        randomAccessFile.seek(range);
        // 每次请求只返回1MB的视频流
        byte[] bytes = new byte[1024 * 1024];
        int len = randomAccessFile.read(bytes);
        //设置此次相应返回的数据长度
        response.setContentLength(len);
        //设置此次相应返回的数据范围
        response.setHeader("Content-Range", "bytes " + range + "-" + (fileLength - 1) + "/" + fileLength);
        // 将这1MB的视频流响应给客户端
        outputStream.write(bytes, 0, len);
        outputStream.close();
        randomAccessFile.close();
        // 输出日志
        log.info("返回数据区间:【" + range + "-" + (range + len) + "】");
    }

    //上传视频缩略图
    @ApiOperation(value = "上传视频缩略图")
    @RequestMapping(value = "{id}/uploadThumbnail", method = RequestMethod.PUT)
    public ResultDTO uploadThumbnail(@RequestPart(value = "thumbnail") MultipartFile thumbnail, @PathVariable("id") Long id) {
        String path = System.getProperty("user.dir");
        String newFileName = "";
        if (thumbnail != null) {
            String fileName = thumbnail.getOriginalFilename();
            assert fileName != null;
            newFileName = UUID.randomUUID() + fileName.substring(fileName.lastIndexOf("."));
            File newFile = new File(path + "/src/main/resources/static/thumbnail/" + newFileName);
            try {
                thumbnail.transferTo(newFile);
            } catch (Exception e) {
                log.info("保存缩略图失败！");
                e.printStackTrace();
                return ResultDTO.error("保存缩略图失败！", null);
            }
            videoService.updateThumb(id, "/thumbnail/" + newFileName);
        } else {
            log.info("上传缩略图失败！");
            return ResultDTO.error("上传缩略图失败！", null);
        }
        return ResultDTO.success("上传缩略图成功！", "http://localhost:8081/video/getThumbnail/thumbnail/" + newFileName);
    }

    //获取视频缩略图
    @ApiOperation(value = "获取视频缩略图")
    @RequestMapping(value = "getThumbnail/thumbnail/{thumbnailName}", method = RequestMethod.GET)
    public ResultDTO getThumbnail(@PathVariable String thumbnailName, HttpServletResponse response) throws IOException {
        try {
            String workpace = System.getProperty("user.dir");
            File file = new File(workpace + "/src/main/resources/static/thumbnail/" + thumbnailName);
            InputStream in = new FileInputStream(file);
            OutputStream out = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
            return ResultDTO.success("获取缩略图成功！");
        } catch (Exception e) {
            return ResultDTO.error("获取缩略图失败！");
        }
    }

    //删除视频
    @ApiOperation(value = "删除视频")
    @DeleteMapping("{id}")
    public ResultDTO deleteVideo(@PathVariable("id") Long id) {
        videoService.deleteVideo(id);
        return ResultDTO.success("删除视频成功！");
    }

    //更新视频信息
    @ApiOperation(value = "更新视频信息")
    @PutMapping("updateVideo")
    public ResultDTO updateVideo(@Nullable @RequestPart(value = "videoFile") MultipartFile videoFile, @RequestBody Video video) {
        if (videoFile != null) {
            String sourceVideo = VideoUtil.uploadVideo(videoFile);
            int dotIndex = sourceVideo.lastIndexOf('.');
            String sourceNameWithoutExt = sourceVideo.substring(0, dotIndex);
            String sourcePath = System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceVideo;
            if (!sourceVideo.toLowerCase().endsWith(".mp4")) {
                String ffmpegPath = System.getenv("FFMPEG_HOME");
                File ffmpegCmd = new File(ffmpegPath + "/bin/ffmpeg.exe");
                FFmpegTranscoder.execVideoTranscode(ffmpegCmd, sourcePath, System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceNameWithoutExt + ".mp4");
                sourcePath = System.getProperty("user.dir") + "/src/main/resources/static/video/" + sourceNameWithoutExt + ".mp4";
            }
            video.setVideoUrl("/video/" + sourceNameWithoutExt + ".mp4");
            video.setDuration(VideoUtil.getVideoDuration(sourcePath));
        } else {
            int i = videoService.updateVideo(video);
            return ResultDTO.success("更新视频信息成功！", video);
        }
        videoService.updateVideo(video);
        return ResultDTO.success("更新视频信息成功！", video);
    }

    //按综合排序获取分页已发布视频列表
    @ApiOperation(value = "按综合排序获取分页已发布视频列表")
    @GetMapping("getVideoList")
    public ResultDTO getVideoList(PageQuery videoQuery) {
        return ResultDTO.success("获取视频列表成功！", videoService.getVideoListByPage(videoQuery));
    }

    //按时间排序获取分页已发布视频列表
    @ApiOperation(value = "按时间排序获取分页已发布视频列表")
    @GetMapping("getVideoListByTime")
    public ResultDTO getVideoListByTime(PageQuery videoQuery) {
        return ResultDTO.success("获取视频列表成功！", videoService.getVideoListByPageOrderByTime(videoQuery));
    }

    //按照热度排序获取分页已发布视频列表
    @ApiOperation(value = "按照热度排序获取分页已发布视频列表")
    @GetMapping("getVideoListByHot")
    public ResultDTO getVideoListByHot(PageQuery videoQuery) {
        return ResultDTO.success("获取视频列表成功！", videoService.getVideoListByPageOrderByHot(videoQuery));
    }

    //视频点赞
    @ApiOperation(value = "视频点赞")
    @PutMapping("{id}/like")
    public ResultDTO like(@PathVariable("id") Long id, HttpServletRequest request) {
        try {
            Message message = new Message();
            Video video = videoService.getVideoById(id);
            message.setMsgTitle("点赞提醒");
            message.setMsgContent("你的视频《" + video.getVideoTitle() + "》收到了用户" + JWTUtil.getPayloadValue(request.getHeader("token"), "name") + "的点赞！");
            message.setMsgSendDate(new Date());
            message.setMsgTypeId(2L);
            message.setMsgVideoId(id);
            message.setMsgReceiveUserId(video.getUserId());
            messageService.save(message);
            videoService.likeVideo(id);
        } catch (Exception e) {
            return ResultDTO.error("点赞失败！");
        }
        return ResultDTO.success("点赞成功！");
    }

    //视频取消点赞
    @ApiOperation(value = "视频取消点赞")
    @PutMapping("{id}/cancelLike")
    public ResultDTO cancelLike(@PathVariable("id") Long id) {
        try {
            videoService.cancelLikeVideo(id);
        } catch (Exception e) {
            return ResultDTO.error("取消点赞失败！");
        }
        return ResultDTO.success("取消点赞成功！");
    }

    //视频收藏
    @ApiOperation(value = "视频收藏", notes = "这里id为视频id,用户需要登录")
    @PutMapping("{id}/collect")
    public ResultDTO collect(@PathVariable("id") Long id, HttpServletRequest request) {
        Collection collection = new Collection();
        Long userId = Long.parseLong(JWTUtil.getPayloadValue(request.getHeader("token"), "id"));
        collection.setVideoId(id);
        collection.setUserId(userId);
        //判断是否已经收藏
        if (collectionService.isCollected(userId, id)) {
            return ResultDTO.error("已经收藏！");
        } else {
            try {
                Message message = new Message();
                Video video = videoService.getVideoById(id);
                message.setMsgTitle("收藏提醒");
                message.setMsgContent("你的视频《" + video.getVideoTitle() + "》收到了用户" + JWTUtil.getPayloadValue(request.getHeader("token"), "name") + "的收藏！");
                message.setMsgSendDate(new Date());
                message.setMsgTypeId(2L);
                message.setMsgReceiveUserId(video.getUserId());
                message.setMsgVideoId(id);
                video.setCollectNum(video.getCollectNum() + 1);
                collectionService.save(collection);
                messageService.save(message);
                videoService.updateVideo(video);
            } catch (Exception e) {
                return ResultDTO.error("收藏失败！");
            }
        }
        return ResultDTO.success("收藏成功！", collectionService.FindIdByUserIdAndVideoId(userId, id));
    }

    //视频取消收藏
    @ApiOperation(value = "视频取消收藏")
    @PutMapping("{id}/cancelCollect")
    public ResultDTO cancelCollect(@PathVariable("id") Long id, HttpServletRequest request) {
        Long userId = Long.parseLong(JWTUtil.getPayloadValue(request.getHeader("token"), "id"));
        //判断是否已经收藏
        if (collectionService.isCollected(userId, id)) {
            try {
                collectionService.remove(new QueryWrapper<Collection>().eq("video_id", id).eq("user_id", userId));
                Video video = videoService.getVideoById(id);
                video.setCollectNum(video.getCollectNum() - 1);
                videoService.updateVideo(video);
            } catch (Exception e) {
                return ResultDTO.error("取消收藏失败！");
            }
        } else {
            return ResultDTO.error("未收藏！");
        }
        return ResultDTO.success("取消收藏成功！");
    }

    //增加视频播放量
    @ApiOperation(value = "增加视频播放量")
    @PutMapping("{id}/addView")
    public ResultDTO addView(@PathVariable("id") Long id) {
        try {
            Video video = videoService.getVideoById(id);
            video.setViewNum(videoService.getVideoById(id).getViewNum() + 1);
            videoService.updateById(video);
        } catch (Exception e) {
            return ResultDTO.error("增加播放量失败！");
        }
        return ResultDTO.success("增加播放量成功！");
    }

    //已发布视频搜索
    @ApiOperation(value = "已发布视频搜索")
    @GetMapping("search/{keyword}")
    public ResultDTO search(PageQuery videoQuery, @PathVariable String keyword) {
        return ResultDTO.success("搜索成功！", videoService.searchVideo(videoQuery, keyword));
    }

    //已发布视频分类搜索
    @ApiOperation(value = "已发布视频分类搜索")
    @GetMapping("searchByCategory/{category}")
    public ResultDTO searchByCategory(PageQuery videoQuery, @PathVariable Long category) {
        return ResultDTO.success("分类搜索成功！", videoService.searchVideoByCategory(videoQuery, category));
    }

    //获取待审核视频列表
    @ApiOperation(value = "获取待审核视频列表")
    @GetMapping("getWaitAuditList")
    public ResultDTO getWaitAuditList(PageQuery videoQuery) {
        try {
            return ResultDTO.success("获取待审核视频列表成功！", videoService.getWaitAuditVideoList(videoQuery));
        } catch (Exception e) {
            return ResultDTO.error("获取待审核视频列表失败！");
        }
    }

    //获取单个视频信息
    @ApiOperation(value = "获取单个视频信息")
    @GetMapping("{id}")
    public ResultDTO getVideoById(@PathVariable("id") Long id) {
        try {
            return ResultDTO.success("获取视频信息成功！", videoService.getVideoById(id));
        } catch (Exception e) {
            return ResultDTO.error("获取视频信息失败！");
        }
    }
}

