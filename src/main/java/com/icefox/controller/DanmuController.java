package com.icefox.controller;

import com.icefox.pojo.Danmu;
import com.icefox.pojo.dto.ResultDTO;
import com.icefox.service.DanmuService;
import com.icefox.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "弹幕管理")
public class DanmuController {
    @Autowired
    private DanmuService danmuService;

    //弹幕发送消息
    @ApiOperation(value = "发送弹幕")
    @RequestMapping(value = "/sendDanmu", method = RequestMethod.POST)
    public ResultDTO sendDanmu(@RequestBody Danmu danmu, HttpServletRequest request) {
        try {
            String token = request.getHeader("token");
            danmu.setUserId(Long.parseLong((JWTUtil.getPayloadValue(token, "id"))));
            danmuService.save(danmu);
            return ResultDTO.success("发送弹幕成功", danmu);
        } catch (Exception e) {
            e.printStackTrace();
            return ResultDTO.error("发送弹幕失败");
        }
    }

    //获取视频弹幕列表
    @ApiOperation(value = "获取视频弹幕列表", notes = "需传入视频id，弹幕的时间，弹幕内容")
    @RequestMapping(value = "{id}/getDanmuList", method = RequestMethod.GET)
    public ResultDTO getDanmuList(@PathVariable("id") Long videoId) {
        try {
            List<Danmu> list = danmuService.lambdaQuery().eq(Danmu::getVideoId, videoId).list();
            return ResultDTO.success("获取弹幕列表成功", list);
        } catch (Exception e) {
            return ResultDTO.error("获取弹幕列表失败");
        }
    }
}
