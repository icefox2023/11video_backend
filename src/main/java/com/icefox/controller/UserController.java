package com.icefox.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.icefox.pojo.User;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.dto.ResultDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.pojo.relation.Focus;
import com.icefox.service.CollectionService;
import com.icefox.service.FocusService;
import com.icefox.service.UserService;
import com.icefox.service.VideoService;
import com.icefox.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.AuthenticationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

// 用户控制器类
@Api(tags = "用户管理")
@RestController
@Slf4j
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CollectionService collectionService;

    @Autowired
    private VideoService videoService;

    @Autowired
    private FocusService focusService;

    // 添加用户
    @ApiOperation("添加用户")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public int addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    // 用户登录
    @ApiOperation("用户登录")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResultDTO<Map<String, Object>> userLogin(@RequestBody User user) {
        log.info("用户[{}]登录", user.getUsername());
        Map<String, Object> result = new HashMap<>();
        try {
            User userDB = userService.login(user);
            if (userDB == null) {
                throw new AuthenticationException("用户名或密码错误！");
            }
            Map<String, String> payload = new HashMap<>();
            payload.put("id", userDB.getUserId().toString());
            payload.put("name", userDB.getUsername());
            // 生成jwt令牌
            String token = JWTUtil.getToken(payload);
            result.put("state", true);
            result.put("msg", "登录成功！");
            result.put("token", token);
            User user1 = userService.getUserById(userDB.getUserId());
            if (user1.getUserState() == 0) {
                result.put("state", false);
                result.put("msg", "用户已被禁用！");
            }
        } catch (AuthenticationException e) {
            result.put("state", false);
            result.put("msg", e.getMessage());
        } catch (Exception e) {
            result.put("state", false);
            result.put("msg", "认证过程中发生错误：" + e.getMessage());
        }
        return ResultDTO.success(result);
    }


    // 根据用户ID获取用户信息
    @ApiOperation("根据用户ID获取用户信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User GetUserById(@PathVariable("id") Long id) {
        return userService.getUserById(id);
    }

    // 更新用户信息
    @ApiOperation("更新用户信息")
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResultDTO UpdateUser(@RequestBody User user, HttpServletRequest request) {
        String token = request.getHeader("token");
        if (token != null && Long.parseLong(JWTUtil.getPayloadValue(token, "id")) == user.getUserId()) {
            userService.updateUser(user);
            return ResultDTO.success("更新用户信息成功！", user);
        } else {
            return ResultDTO.error("更新用户信息失败！");
        }
    }

    // 上传用户头像
    @ApiOperation("上传用户头像")
    @RequestMapping(value = "/{id}/uploadIcon", method = RequestMethod.PUT)
    public ResultDTO updateIcon(@RequestPart(value = "icon") MultipartFile icon, @PathVariable("id") Long id) {
        String path = System.getProperty("user.dir");
        String newFileName = "";
        if (icon != null) {
            String fileName = icon.getOriginalFilename();
            assert fileName != null;
            newFileName = UUID.randomUUID() + fileName.substring(fileName.lastIndexOf("."));
            File newFile = new File(path + "/src/main/resources/static/icon/" + newFileName);
            try {
                icon.transferTo(newFile);
            } catch (Exception e) {
                log.info("保存头像失败！");
                e.printStackTrace();
                return ResultDTO.error("保存头像失败！", null);
            }
            userService.updateIcon("/icon/" + newFileName, id);
        } else {
            log.info("上传头像失败！");
            return ResultDTO.error("上传头像失败！", null);
        }
        return ResultDTO.success("上传头像成功！", "http://localhost:8081/user/getIcon/icon/" + newFileName);
    }

    //获取用户头像
    @ApiOperation("获取用户头像")
    @RequestMapping(value = "/getIcon/icon/{iconName}", method = RequestMethod.GET)
    public ResultDTO getIcon(HttpServletResponse response, @PathVariable String iconName) throws IOException {
        try {
            String workpace = System.getProperty("user.dir");
            File file = new File(workpace + "/src/main/resources/static/icon/" + iconName);
            InputStream in = new FileInputStream(file);
            OutputStream out = response.getOutputStream();
            byte[] buffer = new byte[1024];
            int len = -1;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
            return ResultDTO.success("获取头像成功！");
        } catch (Exception e) {
            return ResultDTO.error("获取头像失败！");
        }
    }

    // 个人视频列表
    @ApiOperation("获取用户视频列表")
    @RequestMapping(value = "/{id}/videos", method = RequestMethod.GET)
    public ResultDTO getVideos(@PathVariable("id") Long id, PageQuery videoQuery) {
        return ResultDTO.success("获取个人视频列表成功", videoService.getVideoListByPageOrderByTime(videoQuery, id));
    }

    //个人草稿视频列表
    @ApiOperation("获取用户草稿视频列表")
    @RequestMapping(value = "/{id}/drafts", method = RequestMethod.GET)
    public ResultDTO getDrafts(@PathVariable("id") Long id, PageQuery videoQuery) {
        return ResultDTO.success("获取个人草稿视频列表成功", videoService.getDraftVideoListByPageOrderByTime(videoQuery, id));
    }

    //个人视频收藏列表
    @ApiOperation("获取用户收藏列表")
    @RequestMapping(value = "/{id}/collections", method = RequestMethod.GET)
    public ResultDTO getCollections(@PathVariable("id") Long id, PageQuery videoQuery) {
        try {
            List<Long> videoIdList = collectionService.FindCollectedVideoIdByUserId(id);
            PageDTO<Video> collectList = PageDTO.of(videoQuery.toMpPage(), Video.class);
            if (videoIdList.size() != 0) {
                collectList = videoService.getCollectVideoListByPageOrderByTime(videoQuery, videoIdList);
            }
            log.info("获取个人收藏列表成功");
            return ResultDTO.success("获取个人收藏列表成功", collectList);
        } catch (Exception e) {
            log.error("获取个人收藏列表失败！", e);
            return ResultDTO.error("获取个人收藏列表失败！");
        }
    }

    //用户列表
    @ApiOperation("获取用户列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResultDTO getUserList(PageQuery userQuery) {
        try {
            log.info("获取用户列表成功");
            return ResultDTO.success("获取用户列表成功", userService.getUserList(userQuery));
        } catch (Exception e) {
            log.error("获取用户列表失败！", e);
            return ResultDTO.error("获取用户列表失败！");
        }
    }

    //用户粉丝列表
    @ApiOperation("获取用户粉丝列表")
    @RequestMapping(value = "/{id}/fans", method = RequestMethod.GET)
    public ResultDTO getFans(@PathVariable("id") Long id, PageQuery userQuery) {
        try {
            List<Long> fansIdList = focusService.getFansIdList(id);
            PageDTO<User> fansList = PageDTO.of(userQuery.toMpPage(), User.class);
            if (fansIdList.size() != 0) {
                fansList = userService.getFansList(userQuery, fansIdList);
            }
            log.info("获取用户粉丝列表成功");
            return ResultDTO.success("获取用户粉丝列表成功", fansList);
        } catch (Exception e) {
            log.error("获取用户粉丝列表失败！", e);
            return ResultDTO.error("获取用户粉丝列表失败！");
        }
    }

    //用户关注列表
    @ApiOperation("获取用户关注列表")
    @RequestMapping(value = "/{id}/follows", method = RequestMethod.GET)
    public ResultDTO getFollows(@PathVariable("id") Long id, PageQuery userQuery) {
        try {
            List<Long> followIdList = focusService.getFocusIdList(id);
            PageDTO<User> followList = PageDTO.of(userQuery.toMpPage(), User.class);
            if (followIdList.size() != 0) {
                followList = userService.getFollowList(userQuery, followIdList);
            }
            log.info("获取用户关注列表成功");
            return ResultDTO.success("获取用户关注列表成功", followList);
        } catch (Exception e) {
            log.error("获取用户关注列表失败！", e);
            return ResultDTO.error("获取用户关注列表失败！");
        }
    }

    //关注功能
    @ApiOperation("关注用户")
    @RequestMapping(value = "/follow", method = RequestMethod.POST)
    public ResultDTO follow(@RequestBody Focus focus, HttpServletRequest request) {
        String token = request.getHeader("token");
        if (token != null && Long.parseLong(JWTUtil.getPayloadValue(token, "id")) == focus.getUserId()) {
            focusService.save(focus);
            User user = userService.getById(focus.getFocusedId());
            user.setFanNum(userService.getById(focus.getFocusedId()).getFanNum() + 1);
            userService.updateById(user);
            return ResultDTO.success("关注成功！");
        } else {
            return ResultDTO.error("关注失败！");
        }
    }

    //取消关注功能
    @ApiOperation("取消关注用户")
    @RequestMapping(value = "/unfollow", method = RequestMethod.POST)
    public ResultDTO unfollow(@RequestBody Focus focus, HttpServletRequest request) {
        String token = request.getHeader("token");
        if (token != null && Long.parseLong(JWTUtil.getPayloadValue(token, "id")) == focus.getUserId()) {
            focusService.remove(new QueryWrapper<Focus>().eq("user_id", focus.getUserId()).eq("focused_id", focus.getFocusedId()));
            User user = userService.getById(focus.getFocusedId());
            user.setFanNum(userService.getById(focus.getFocusedId()).getFanNum() - 1);
            userService.updateById(user);
            return ResultDTO.success("取消关注成功！");
        } else {
            return ResultDTO.error("取消关注失败！");
        }
    }

    //是否关注
    @ApiOperation("是否关注")
    @RequestMapping(value = "{id}/isFollow", method = RequestMethod.GET)
    public ResultDTO isFollow(@PathVariable("id") Long userId, Long focusedId) {
        try {
            boolean isFollow = focusService.isFocus(userId, focusedId);
            return ResultDTO.success("是否关注！", isFollow);
        } catch (Exception e) {
            log.error("请求关注状态失败！", e);
            return ResultDTO.error("请求关注状态失败！");
        }
    }
}
