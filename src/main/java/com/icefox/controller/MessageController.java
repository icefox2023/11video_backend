package com.icefox.controller;

import com.icefox.pojo.Message;
import com.icefox.pojo.Video;
import com.icefox.pojo.dto.PageDTO;
import com.icefox.pojo.dto.ResultDTO;
import com.icefox.pojo.query.PageQuery;
import com.icefox.service.MessageService;
import com.icefox.service.VideoService;
import com.icefox.util.JWTUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/message")
@Api(tags = "消息管理")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private VideoService videoService;

    //添加评论
    @ApiOperation(value = "添加评论", notes = "这里id为视频id,这里只需要传入评论内容即可")
    @RequestMapping(value = "{id}/addComment", method = RequestMethod.POST)
    public ResultDTO addComment(@RequestBody Message message, @PathVariable Long id, HttpServletRequest request) {
        try {
            message.setMsgTypeId(3L);
            message.setMsgVideoId(id);
            message.setMsgTitle("视频评论");
            message.setMsgSendDate(new Date());
            message.setMsgSendUserId(Long.parseLong((JWTUtil.getPayloadValue(request.getHeader("token"), "id"))));
            messageService.save(message);
            Video video = videoService.getVideoById(id);
            video.setCommentNum(video.getCommentNum() + 1);
            videoService.updateVideo(video);
            return ResultDTO.success("添加评论成功");
        } catch (Exception e) {
            return ResultDTO.error("添加评论失败");
        }
    }

    //获取视频评论
    @ApiOperation(value = "获取视频评论", notes = "这里id为视频id")
    @RequestMapping(value = "{id}/getComments", method = RequestMethod.GET)
    public ResultDTO getComments(PageQuery pageQuery, @PathVariable Long id) {
        try {
            PageDTO<Message> comments = messageService.getComments(pageQuery, id);
            return ResultDTO.success("获取评论成功", comments);
        } catch (Exception e) {
            return ResultDTO.error("获取评论失败");
        }
    }

    //获取系统消息
    @ApiOperation(value = "用户获取系统消息", notes = "这里id为用户id")
    @RequestMapping(value = "{id}/getSystemMessages", method = RequestMethod.GET)
    public ResultDTO getSystemMessages(PageQuery pageQuery, @PathVariable Long id) {
        try {
            PageDTO<Message> systemMessages = messageService.getSystemMessages(pageQuery, id);
            return ResultDTO.success("获取系统消息成功", systemMessages);
        } catch (Exception e) {
            return ResultDTO.error("获取系统消息失败");
        }
    }

    //评论删除
    @ApiOperation(value = "评论删除")
    @RequestMapping(value = "{id}/deleteComment", method = RequestMethod.DELETE)
    public ResultDTO deleteComment(@PathVariable Long id, HttpServletRequest request) {
        if (id != Long.parseLong(JWTUtil.getPayloadValue(request.getHeader("token"), "id"))) {
            return ResultDTO.error("只能删除自己的评论");
        }
        try {
            messageService.deleteComment(id);
            return ResultDTO.success("评论删除成功");
        } catch (Exception e) {
            return ResultDTO.error("评论删除失败");
        }
    }

    //私信查看
    @ApiOperation(value = "私信查看")
    @RequestMapping(value = "getMessages", method = RequestMethod.GET)
    public ResultDTO getMessages(PageQuery pageQuery, @RequestParam(value = "toUserId") Long toUserId, HttpServletRequest request) {
        try {
            String token = request.getHeader("token");
            Long fromUserId = Long.parseLong(JWTUtil.getPayloadValue(token, "id"));
            PageDTO<Message> messages = messageService.getChatMessages(pageQuery, fromUserId, toUserId);
            return ResultDTO.success("私信查看成功", messages);
        } catch (Exception e) {
            return ResultDTO.error("私信查看失败");
        }
    }
}
