package com.icefox.websocketservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.icefox.pojo.Message;
import com.icefox.service.MessageService;
import com.icefox.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@ServerEndpoint("/chat/{id}")
//注意这里的id参数类型为登录用户的ID
public class ChatServe {

    public static MessageService messageService;

    public static UserService userService;

    @Autowired
    public void setMessageService(MessageService messageService) {
        ChatServe.messageService = messageService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        ChatServe.userService = userService;
    }

    private static Map<Long, Session> sessions = new HashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("id") Long senderId) {
        try {
            if (!sessions.containsKey(senderId)) {
                sessions.put(senderId, session);
            }
            log.info("用户ID[{}]:{}已连接", senderId, userService.getUserById(senderId).getUsername());
        } catch (Exception e) {
            log.error("WebSocket连接失败", e);
        }
    }

    @OnMessage
    public void onMessage(String message, @PathParam("id") Long senderId) {
        //私信发送
        Message msg = new Message();
        ObjectMapper mapper = new ObjectMapper();
        try {
            msg = mapper.readValue(message, Message.class);
        } catch (Exception e) {
            log.error("用户{}私信消息解析失败", senderId, e);
            return;
        }
        msg.setMsgTitle("私信");
        msg.setMsgSendUserId(senderId);
        msg.setMsgSendDate(new Date());
        msg.setMsgTypeId(1L);
        try {
            messageService.save(msg);
        } catch (Exception e) {
            log.error("私信保存失败", e);
            return;
        }
        log.info("私信发送成功，接收方ID：{}，消息内容：{}", msg.getMsgReceiveUserId(), msg.getMsgContent());
        Session receiverSession = sessions.get(msg.getMsgReceiveUserId());
        if (receiverSession != null) {
            receiverSession.getAsyncRemote().sendText(message);
        } else {
            log.info("接收方ID：{}的WebSocket未连接", msg.getMsgReceiveUserId());
        }
    }

    @OnClose
    public void onClose(@PathParam("id") Long senderId) {
        try {
            sessions.remove(senderId);
            log.info("发送方ID：{}已断开连接", senderId);
        } catch (Exception e) {
            log.error("WebSocket断开连接失败", e);
        }
    }

    @OnError
    public void onError(Throwable error) {
        log.error("WebSocket发生错误", error);
    }

}
