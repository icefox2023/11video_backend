package com.icefox.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "弹幕")
@TableName("danmu")
public class Danmu {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "弹幕ID")
    private Long danmuId;
    @ApiModelProperty(value = "视频ID")
    private Long videoId;
    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "弹幕内容")
    private String danmuContent;
    @ApiModelProperty(value = "弹幕时间")
    private Long progressTime;
}
