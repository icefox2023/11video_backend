package com.icefox.pojo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * video
 *
 * @author
 */
@Data
@TableName(value = "video")
@ApiModel(value = "视频")
public class Video implements Serializable {
    /**
     * 视频id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "视频id")
    private Long videoId;

    /**
     * 视频标题
     */
    @ApiModelProperty(value = "视频标题")
    private String videoTitle;

    /**
     * 视频简介
     */
    @ApiModelProperty(value = "视频简介")
    private String videoInfo;

    /**
     * 编辑日期
     */
    @ApiModelProperty(value = "编辑日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date editDate;

    /**
     * 视频路径
     */
    @ApiModelProperty(value = "视频路径")
    private String videoUrl;

    /**
     * 视频缩略图
     */
    @ApiModelProperty(value = "视频缩略图")
    private String thumbnailUrl;

    /**
     * 视频状态
     */
    @ApiModelProperty(value = "视频状态")
    private Long videoStateId;

    /**
     * 观看数
     */
    @ApiModelProperty(value = "观看数")
    private Integer viewNum;

    /**
     * 点赞数
     */
    @ApiModelProperty(value = "点赞数")
    private Integer likeNum;

    /**
     * 收藏数
     */
    @ApiModelProperty(value = "收藏数")
    private Integer collectNum;

    /**
     * 评论数
     */
    @ApiModelProperty(value = "评论数")
    private Integer commentNum;

    /**
     * 视频类型id
     */
    @ApiModelProperty(value = "视频类型id")
    private Long videoTypeId;

    /**
     * 发布视频的用户id
     */
    @ApiModelProperty(value = "发布视频的用户id")
    private Long userId;

    /**
     * 视频时长
     */
    @ApiModelProperty(value = "视频时长")
    private Long duration;

}