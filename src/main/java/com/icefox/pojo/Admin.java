package com.icefox.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * admin
 *
 * @author jjy
 */
@Data
@ApiModel(value = "管理员")
@TableName("admin")
public class Admin {
    /**
     * 管理id
     */
    @ApiModelProperty(value = "管理id")
    @TableId(type = IdType.AUTO)
    private Long adminId;

    /**
     * 管理员用户名
     */
    @ApiModelProperty(value = "管理员用户名")
    private String adminUsername;

    /**
     * 管理员密码
     */
    @ApiModelProperty(value = "管理员密码")
    private String adminPassword;
}