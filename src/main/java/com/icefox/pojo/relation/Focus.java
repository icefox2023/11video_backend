package com.icefox.pojo.relation;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@TableName("focus")
@ApiModel(value = "关注")
public class Focus {
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "关注id")
    private Long focusId;
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "关注的用户id")
    private Long focusedId;
}
