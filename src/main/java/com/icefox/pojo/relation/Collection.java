package com.icefox.pojo.relation;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("user_collection_record")
public class Collection {
    @TableId(type = IdType.AUTO)
    private Long collectionId;
    private Long userId;
    private Long videoId;
}
