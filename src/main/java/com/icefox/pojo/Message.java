package com.icefox.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName(value = "message")
@ApiModel(value = "消息")
public class Message {

    // 消息ID
    @ApiModelProperty(value = "消息ID", example = "123456789")
    @TableId(type = IdType.AUTO)
    private Long msgId;

    // 消息标题
    @ApiModelProperty(value = "消息标题", example = "Hello, World!")
    private String msgTitle;

    // 消息内容
    @ApiModelProperty(value = "消息内容", example = "这是一条测试消息。")
    private String msgContent;

    // 消息发送日期
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "消息发送日期", example = "2023-07-18 15:30:00")
    private Date msgSendDate;

    // 发送消息的用户ID
    @ApiModelProperty(value = "发送消息的用户ID", example = "987654321")
    private Long msgSendUserId;

    // 接收消息的用户ID
    @ApiModelProperty(value = "接收消息的用户ID", example = "1122334455")
    private Long msgReceiveUserId;

    // 消息状态ID
    @ApiModelProperty(value = "消息状态ID", example = "1")
    private Long msgStateId;

    // 消息类型ID
    @ApiModelProperty(value = "消息类型ID", example = "2")
    private Long msgTypeId;

    // 视频ID
    @ApiModelProperty(value = "视频ID", example = "123456789")
    private Long msgVideoId;
}

