package com.icefox.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

//get与set方法
@Data
//全参构造
@AllArgsConstructor
//无参构造
@NoArgsConstructor
//表名
@TableName("user")
//tostring方法
@ToString
@ApiModel(value = "用户")
public class User implements Serializable {

    @ApiModelProperty(value = "用户ID，自动生成")
    @TableId(type = IdType.AUTO)
    private Long userId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户密码")
    private String password;

    @ApiModelProperty(value = "用户电话号码")
    private String userTel;

    @ApiModelProperty(value = "用户年龄")
    private Integer age;

    @ApiModelProperty(value = "用户性别")
    private String sex;

    @ApiModelProperty(value = "用户头像URL")
    private String iconUrl;

    @ApiModelProperty(value = "用户生日")
    private String userBirth;

    @ApiModelProperty(value = "用户状态")
    private Long userState;

    @ApiModelProperty(value = "粉丝数量")
    private Integer fanNum;

    @ApiModelProperty(value = "用户签名")
    private String signature;
}
