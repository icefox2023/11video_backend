package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.relation.Focus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface FocusMapper extends BaseMapper<Focus> {
    //获取关注id列表
    @Select("SELECT focused_id FROM focus WHERE user_id = #{userId}")
    List<Long> getFocusIdList(Long userId);

    //获取粉丝id列表
    @Select("SELECT user_id FROM focus WHERE focused_id = #{focusedId}   ")
    List<Long> getFanIdList(Long focusedId);

    //是否关注
    @Select("SELECT COUNT(*) FROM focus WHERE user_id = #{userId} AND focused_id = #{focusedId}")
    Boolean isFocus(@Param("userId") Long userId, @Param("focusedId") Long focusedId);
}
