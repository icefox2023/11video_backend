package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.Danmu;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DanmuMapper extends BaseMapper<Danmu> {
}
