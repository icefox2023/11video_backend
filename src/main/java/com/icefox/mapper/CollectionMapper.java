package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.relation.Collection;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CollectionMapper extends BaseMapper<Collection> {
    @Select("SELECT EXISTS(SELECT 1 FROM user_collection_record WHERE user_id = #{userId} AND video_id = #{videoId})")
    boolean isCollected(@Param("userId") Long userId, @Param("videoId") Long videoId);

    @Select("SELECT * FROM user_collection_record WHERE user_id = #{userId} AND video_id = #{videoId}")
    Collection findCollectionIdByUserIdAndVideoId(@Param("userId") Long userId, @Param("videoId") Long videoId);

    @Select("SELECT video_id FROM user_collection_record WHERE user_id = #{userId}")
    List<Long> findCollectedVideoIdByUserId(@Param("userId") Long userId);
}

