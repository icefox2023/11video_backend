package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface VideoMapper extends BaseMapper<Video> {

    @Update("UPDATE video SET thumbnail_url = #{thumb} WHERE video_id = #{id}")
    int updateThumb(@Param("id") Long id, @Param("thumb") String thumb);

}