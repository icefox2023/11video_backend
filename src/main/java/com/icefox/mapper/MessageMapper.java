package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.Message;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MessageMapper extends BaseMapper<Message> {
}
