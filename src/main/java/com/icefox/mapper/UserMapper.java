package com.icefox.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.icefox.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select("SELECT * FROM user WHERE username = #{username} AND password = #{password}")
    User login(User user);

    //只使用一个参数：如果你的Mapper方法只接受一个参数，那么你可以直接使用该参数，而无需任何额外的注解或配置。MyBatis会自动将参数值绑定到SQL语句中。
    //使用#{0}, #{1}, ...：当你的Mapper方法接受多个参数时，并且你不想使用@Param注解，你可以使用#{0}, #{1}, ...这样的占位符来引用参数。这些占位符是按照参数在方法签名中出现的顺序来引用的。
    @Update("UPDATE user SET icon_url = #{icon} WHERE user_id = #{id}")
    int updateIcon(@Param("icon") String icon, @Param("id") Long id);
}
