package com.icefox.interceptor;

import com.icefox.util.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {

        String requestURL = request.getRequestURL().toString();
        log.info("请求的url:{}", requestURL);

//        //排除预检请求
//        if (request.getMethod().equals("OPTIONS")) {
//            log.info("预检请求，放行");
//            return true;
//        }

        if (requestURL.contains("/login")) {
            log.info("登录操作，放行");
            return true;
        }

        String token = request.getHeader("token");

        if (!StringUtils.hasLength(token)) {
            log.warn("请求头token为空，返回未登录的信息");
            response.getWriter().write("未登录，请先登录");
            return false;
        }

        boolean tokenValid = JWTUtil.isTokenValid(token);
        if (!tokenValid) {
            log.warn("错误的token，返回未登录的信息");
            response.getWriter().write("错误的token，请重新登录");
            return false;
        }

        return true;
    }
}
